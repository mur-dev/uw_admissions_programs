/**
 * @file
 * Javascript for admissions_programs
 */

var request, user_ip, user_city, user_region, user_country, search_id;

function get_url_parameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||'';
}

(function ($) {
	$(document).ready(function(){
/*		$.get("http://ipinfo.io", function(response) {
			user_ip = response.ip;
			user_city = response.city;
			user_region = response.region;
			user_country = response.country;
		}, "jsonp")
		.always(function () {
*/
			if (request)
				request.abort();
	
			request = $.ajax({
				url: "results/query/analytic_keywords",
				type: "get",
				data: {
					keyword: get_url_parameter('query'),
					grad: 0,
					undergrad: 1,
					ip: ''/*user_ip*/,
					city: ''/*user_city*/,
					region: ''/*user_region*/,
					country: ''/*user_country*/
				}
			});
	
			request.done(function (response, textStatus, jqXHR){
				//console.log(response[0]["search_id"]);
				search_id = response[0]["search_id"];
			});
//		});
	
		$(".result a").click(function(){
			if (search_id != null){
				if (request)
					request.abort();
	
				request = $.ajax({
					url: "results/query/analytic_tracking",
					type: "get",
					data: {
						search_id: search_id,
						program_name: $(this).html(),
						program_url: $(this).attr("href"),
					},
				});
			}
		});
		
	});
})(jQuery);