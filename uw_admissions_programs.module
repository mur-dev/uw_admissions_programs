<?php
/**
 * @file
 * Admissions program module for UW. Student will be able to view all the programs offered by UW as well as search the programs.
 */

/**
 * Implements hook_menu().
 */
function uw_admissions_programs_menu() {
  $items = array();
  //Landing Page
  $items['programs'] = array(
    'title' => 'Programs',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_admissions_programs_display'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => "main-menu",
    'weight' => 100,
  );

  //Results Page
  $items['programs/results'] = array(
    'title' => 'Search results',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_admissions_programs_form'),
    'access arguments' => array('access content'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => "main-menu",
    'weight' => 100,
  );
  //Admin Page
  $items['admin/config/programs_landing'] = array(
    'title' => 'Programs Landing Page Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('uw_admissions_programs_admin'),
    'type' => MENU_NORMAL_ITEM,
    'menu_name' => 'menu-site-management',
    'access callback' => 'uw_admissions_programs_permissions',
    'access arguments' => array(array('site manager', 'administrator')),
  );
  return $items;
}

/**
 * Custom access check based on role permissions
 */
function uw_admissions_programs_permissions($roles = array()) {
    global $user;
    foreach ($roles as $role) {
      if (in_array($role, array_values($user->roles))) {
        return TRUE;
      }
      else {
        return FALSE;
      }
    }
}

/**
 * Returns result
 */
function uw_admissions_programs_form($form_state) {
  $path = drupal_get_path('module', 'uw_admissions_programs');
  drupal_add_css($path . '/css/style.css');
  drupal_add_js($path . '/js/global.js');
  db_set_active('mur');

  $keyword = (empty($_GET['query']) ? '' : $_GET['query']);

  $results = db_query("CALL ADMISSIONS_PROGRAM_SEARCH(:program_level, :program_keyword);", array(
  ':program_level' => 1,
  ':program_keyword' => $keyword
  ));

  $results = $results->fetchAll();
  $rescnt=count($results);

  if (!empty($keyword)) {
    if ($rescnt > 0) {
      $form['queryInfo'] = array(
        '#type' => 'markup',
        '#markup' => t('<br><h3>Here\'s what we found related to "<strong>@query</strong>"</h3>', array('@query' => $keyword))
      );
    }
    else {
      $form['queryInfo'] = array(
      '#type' => 'markup',
      '#markup' => t('<br><h3>Sorry, we didn\'t find anything related to "<strong>@query</strong>"</h3>', array('@query' => $keyword))
      );
    }
  }

  if (empty($keyword)) {
    $form['generalInfo'] = array(
      '#type' => 'markup',
      '#markup' => t("<p class='info-p'>You can also view programs grouped <a href='@link1'>by theme</a> or <a href='@link2'>by faculty</a>.</p>", array('@link1' => url('https://uwaterloo.ca/find-out-more/programs-themes'), '@link2' => url('https://uwaterloo.ca/find-out-more/programs-faculty')))
    );
  }

  $programs_list = "";
  $easy_nav = array();
  foreach ($results as $row) {
    $programs_list .= "<li class='result'><a href='" . $row->program_url . "' name='" . substr($row->program_name, 0, 1) . "'>" . $row->program_name . "</a>";
    $programs_list .= "</li>";
    array_push($easy_nav, substr($row->program_name, 0, 1));
  }

  $easy_nav = array_unique($easy_nav);

  $easy_nav_string = "";
  foreach ($easy_nav as $letter) {
    $easy_nav_string .= "<li><a href='#" . $letter . "'>" . $letter . "</a></li>";
  }

  $form['programList'] = array(
    '#type' => 'markup',
    '#markup' => t("<ul>" . $programs_list . "</ul>")
  );

  if (!empty($keyword) && ($rescnt > 0)) {
    $form['generalInfo'] = array(
      '#type' => 'markup',
      '#markup' => t("<p class='info-p'>You can also view programs grouped <a href='@link1'>by theme</a> or <a href='@link2'>by faculty</a>.</p><br>", array('@link1' => url('https://uwaterloo.ca/find-out-more/programs-themes'), '@link2' => url('https://uwaterloo.ca/find-out-more/programs-faculty')))
    );
  }

  if (empty($keyword)) {
    $form['search_keyword'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#size' => 35,
      '#attributes' => array('placeholder' => t('Search programs or areas of interest')),
      '#prefix' => t('<h5>Search using keywords</h5><div id="search-bar">'),
      '#suffix' => '</div>',
    );
  }
  elseif ($rescnt > 0) {
    $form['search_keyword'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#size' => 35,
      '#attributes' => array('placeholder' => t('Search programs or areas of interest')),
      '#prefix' => t('<h5>Search for another program</h5><div id="search-bar">'),
      '#suffix' => '</div>',
    );
  }
  else {
  $form['search_keyword'] = array(
      '#type' => 'textfield',
      '#required' => FALSE,
      '#size' => 35,
      '#attributes' => array('placeholder' => t('Search programs or areas of interest')),
      '#prefix' => t('<h5>You can <a href="?">view all programs</a> or search for another program.</h5><div id="search-bar">'),
    );
  }

  $form['search_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Search'),
    '#suffix' => '</div>'
  );

  if (!empty($keyword) && ($rescnt > 0)) {
    $form['view_all'] = array(
      '#type' => 'submit',
      '#name' => 'allprograms',
      '#value' => t('View all programs'),
      '#submit' => array('uw_admissions_programs_all_programs'),
      '#prefix' => '<div id="button-container">',
      '#suffix' => '</div>'
    );
  }
  else {
    $form['button-container-close'] = array(
      '#type' => 'markup',
    );
  }

  db_set_active();
  return $form;
}

/**
 * Function for validation if needed
 */
function uw_admissions_programs_form_validate($form, &$form_state) {
}

/**
 * Submits form
 */
function uw_admissions_programs_form_submit($form, &$form_state) {
  $query = $form_state['values']['search_keyword'];
  $options = array('query' => array('query' => $query));
  drupal_goto('programs/results', $options);
}

/**
 * Programs per region?
 */
function uw_admissions_programs_preprocess_region(&$variables) {
  $region = $variables['region'];

  if (in_array($region, array('content'))) {
    $variables['classes_array'][] = 'wide';
  }
}

/**
 * Displays main page
 */
function uw_admissions_programs_display($form_state) {
  $path = drupal_get_path('module', 'uw_admissions_programs');
  drupal_add_css($path . '/css/style_landing.css');

  $rand_pick = rand(1, 3);
  $file = file_load(variable_get('uw_admissions_programs_search_fid_' . $rand_pick,''));
  if (isset($file->uri)) {
    $uri = $file->uri;
    $url = file_create_url($uri);
  }

  $form['promotional_banner'] = array(
    '#markup' =>  '<br><div id="banner-div"><img style="padding-top:20px;" src=' . $url . ' /></div>',
    '#weight' => 100,
  );

  $form['search_keyword'] = array(
    '#type' => 'textfield',
    '#required' => FALSE,
    '#size' => 30,
    '#attributes' => array('placeholder' => t('Search programs or areas of interest')),
  );

  $form['search_submit'] = array(
    '#type' => 'submit',
    '#value' => t('search'),
    '#suffix' => '</div>'
  );

  $form['view_all'] = array(
    '#type' => 'submit',
    '#name' => 'allprograms',
    '#value' => t('view all programs'),
    '#submit' => array('uw_admissions_programs_all_programs'),
  '#prefix' => t('<div id="buttons-container"> <div class="buttons" id="view_all_button">'),
    '#suffix' => t('</div>'),
  );

  $form['view_all_faculty'] = array(
    '#type' => 'submit',
    '#name' => 'allprograms_faculty',
    '#value' => t('Programs by faculty'),
    '#submit' => array('uw_admissions_programs_all_programs_faculty'),
    '#prefix' => t('<div class="buttons" id="view_all_faculty_button">'),
    '#suffix' => t('</div>'),
  );

  $form['view_all_themes'] = array(
    '#type' => 'submit',
    '#name' => 'allprograms_theme',
    '#value' => t('Programs by topic'),
    '#submit' => array('uw_admissions_programs_all_programs_themes'),
    '#prefix' => t('<div class="buttons" id="view_all_themes_button">'),
    '#suffix' => t('</div> </div>'),
  );

  return $form;
}

/**
 * Validates form if needed
 */
function uw_admissions_programs_display_validate($form, &$form_state) {
}

/**
 * Submits form
 */
function uw_admissions_programs_display_submit($form, &$form_state) {
  $query = $form_state['values']['search_keyword'];
  $query = strtolower($query);
  $options = array('query' => array('query' => $query));
  drupal_goto('programs/results', $options);
}

/**
 * Redirects to all programs
 */
function uw_admissions_programs_all_programs() {
  drupal_goto('programs/results');
}

/**
 * Redirects to programs by faculty
 */
function uw_admissions_programs_all_programs_faculty() {
  drupal_goto('/programs-faculty');
}

/**
 * Redirects to programs by theme
 */
function uw_admissions_programs_all_programs_themes() {
  drupal_goto('/programs-themes');
}

/**
 * Three banners/images
 */
function uw_admissions_programs_admin() {
  $form = array();
  $form['uw_admissions_programs_search_fid_1'] = array(
    '#type' => 'managed_file',
    '#required' => TRUE,
    '#title' => t('Promotional Banner 1'),
    '#default_value' => variable_get('uw_admissions_programs_search_fid_1', ''),
    '#description' => t('Upload an image'),
    '#upload_location' => 'public://admissions-files/images',
  );
  $form['uw_admissions_programs_search_fid_2'] = array(
    '#type' => 'managed_file',
    '#required' => TRUE,
    '#title' => t('Promotional Banner 2'),
    '#default_value' => variable_get('uw_admissions_programs_search_fid_2', ''),
    '#description' => t('Upload an image'),
    '#upload_location' => 'public://admissions-files/images',
  );
  $form['uw_admissions_programs_search_fid_3'] = array(
    '#type' => 'managed_file',
    '#required' => TRUE,
    '#title' => t('Promotional Banner 3'),
    '#default_value' => variable_get('uw_admissions_programs_search_fid_3', ''),
    '#description' => t('Upload an image'),
    '#upload_location' => 'public://admissions-files/images',
  );


  $form['#submit'][] = 'uw_admissions_programs_admin_submit';

  return system_settings_form($form);
}

/**
 * Implements custom submit form
 */
function uw_admissions_programs_admin_submit($form, &$form_state) {
  for ($x = 1; $x <= 3; $x++) {
    if ($form_state['values']['uw_admissions_programs_search_fid_' . $x] != 0) {
      $file = file_load($form_state['values']['uw_admissions_programs_search_fid_' . $x]);
      $file->status = FILE_STATUS_PERMANENT;
      file_save($file);
      file_usage_add($file, 'uw_admissions_programs', 'uw_admissions_programs_search', 1);
      variable_set('uw_admissions_programs_search_fid_' . $x, $file->fid);
    }
    elseif ($form_state['values']['uw_admissions_programs_search_fid_' . $x] == 0) {
      $fid = variable_get('uw_admissions_programs_search_fid', FALSE);
      $file = $fid ? file_load($fid) : FALSE;
      if ($file) {
        file_usage_delete($file, 'uw_admissions_programs', 'uw_admissions_programs_search', 1);
        file_delete($file);
      }
      variable_set('uw_admissions_programs_search_fid_' . $x, FALSE);
    }
}
}
